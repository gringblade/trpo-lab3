﻿using System;

namespace TRPO_Lab3.Lib
{
    public class Formuli
    {
        public static double PloshadCircle(double R, double r )
        {
            const double pi = 3.14;
            double S = pi * ((R * R) - (r * r));
            return S;
        }
        public static double DivNull(int a, int b)
        {
            return a / b;
        }
    }
}
