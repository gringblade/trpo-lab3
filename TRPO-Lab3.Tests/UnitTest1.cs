using NUnit.Framework;
using System;
using TRPO_Lab3.Lib;

namespace TRPO_Lab3.Tests
{
    public class Tests
    {
        [Test]
        public void TestSquare()
        {
            double R = 5;
            double r = 3;
            double result = 50.24;
            var S = TRPO_Lab3.Lib.Formuli.PloshadCircle(R, r);
            Assert.AreEqual(result, S, 0.1);
        }
        [Test]
        public void TestException()
        {
            // arrange
            int a = 5;
            int b = 0;
            // act

            // assert
            Assert.Throws<DivideByZeroException>(() => Formuli.DivNull(a, b));
        }
    }
}

