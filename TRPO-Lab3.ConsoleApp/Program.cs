﻿using System;
using TRPO_Lab3.Lib;

namespace TRPO_Lab3.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите R");
            double R = double.Parse(Console.ReadLine());
            Console.WriteLine("Введите r");
            double r = double.Parse(Console.ReadLine());
            var S = Formuli.PloshadCircle(R, r);
            Console.WriteLine("Площадь кольца = " + S);
        }
    }
}
